export declare type ContributorReferenceDatum = {
    fields: [
        { "int": number},
        { "list": [{"bytes": string}]}
    ],
    constructor: 0
}

export declare type ContributorRecord = {
    contributorDatum: ContributorReferenceDatum,
    alias: string
}