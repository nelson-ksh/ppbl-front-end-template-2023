import SLTs203 from "@/src/components/course-modules/203/203-SLTs";
import Lesson2031 from "@/src/components/course-modules/203/Lesson-2031";
import Lesson2032 from "@/src/components/course-modules/203/Lesson-2032";
import Lesson2033 from "@/src/components/course-modules/203/Lesson-2033";
import Lesson2034 from "@/src/components/course-modules/203/Lesson-2034";
import Assignment2031Page from "@/src/components/course-modules/203/Assignment2031";
import ModuleLessons from "@/src/components/lms/Lesson/Lesson";
import slt from "@/src/data/slts-english.json";
import Summary203 from "@/src/components/course-modules/203/Summary203";
import Status203 from "@/src/components/course-modules/203/Status203";
import NFTGalleryPage from "@/src/components/course-modules/203/NFT-gallery";

const Module203Lessons = () => {
  const moduleSelected = slt.modules.find((m) => m.number === 203);
  
  const lessons = [
    {
      key: "slts",
      component: (
        <>
          <SLTs203 />
        </>
      ),
    },
    { key: "2031", component: <Lesson2031 /> },
    { key: "2032", component: <Lesson2032 /> },
    { key: "2033", component: <Lesson2033 /> },
    { key: "2034", component: <Lesson2034 /> },
    { key: "assignment2031", component: <Assignment2031Page /> },
    { key: "nft-gallery", component: <NFTGalleryPage /> },
    { key: "summary", component: <Summary203 /> },
  ];

  return (
    <ModuleLessons
      items={moduleSelected?.lessons ?? []}
      modulePath="/modules/203"
      selected={0}
      lessons={lessons}
      status={<Status203 />}
    />
  );
};

export default Module203Lessons;
